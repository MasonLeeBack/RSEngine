/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: Game.cpp

*/

#include "RSEngine.h"

Game* g_Game;
bool g_GameActive = true;
RENDERMODE renderMode;

bool Game::Init()
{
	// Just set up the global and stuff
	Game* l_Game = new Game;
	g_Game = l_Game;

	return g_Game->InitPhase1();
}

//
// Don't be fooled! We actually initialize the engine here.
//
bool Game::InitPhase1()
{

	//
	// Initialize all of the systems we need.
	//
	// Notice how these are all pointers? The classes
	// automatically convert themselves to pointers! Rad!
	//

	Filesystem l_Filesystem;
	if (!l_Filesystem.Init())
	{
		return false;
	}

	Input l_Input;
	if (!l_Input.Init())
	{
		return false;
	}

	rs::Renderer = new rs::Render;
	if (!Renderer_Initialize())
	{
		return false;
	}

	Level l_Level;
	if (!l_Level.Init())
	{
		return false;
	}

	ScriptService l_ScriptService;
	if (!l_ScriptService.Init())
	{
		return false;
	}
	
	// Load a test level until we have a properly implemented level system.
	g_Level->LoadTestLevel();

	Game::GameLoop();

	// Shut down after the game loop ends.
	Game::Shutdown();

	return true;
}

//
// This is a CROSS_PLATFORM function. Be wary.
//
int Game::GameLoop()
{
#ifdef _WIN32
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				/*
				if (PolySendExit == TRUE) {
					PolyShutdown();
					break;
				}
				*/
				break;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			g_ScriptService->Update();
			g_Input->Update();
			Renderer_RenderScene();

		}
	}

	return (int)msg.wParam;
#endif
	return 0;
}

void Game::Shutdown()
{
	//
	// Make sure to call the other shutdowns too.
	//
	g_Level->Unload();
	g_ScriptService->Shutdown();
	g_Input->Shutdown();
	g_Filesystem->Shutdown();
	Renderer_Shutdown();

	delete g_Game;
	g_Game = 0;
}