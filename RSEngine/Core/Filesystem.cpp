/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: Filesystem.cpp

*/

#include <RSEngine.h>

Filesystem* g_Filesystem;

bool Filesystem::Init()
{
	Filesystem* l_Filesystem = new Filesystem;
	g_Filesystem = l_Filesystem;

	return g_Filesystem->Phase1Init();
}

void Filesystem::Shutdown()
{
	delete g_Filesystem;
	g_Filesystem = 0;
}

char* Filesystem::ReadFile(const char* FileName)
{
	char* Result = "";
	std::ifstream inFile;

	inFile.open(FileName);
	if (!inFile) {
		return nullptr;
	}

	while (!inFile.eof())
		for (int i = 0; i <= 255; i++)
			Result[i] = inFile.get();

	return Result;
}

//
// To do: read .pak files and treat them like normal directories
//

bool Filesystem::Phase1Init()
{
	return true;
}
