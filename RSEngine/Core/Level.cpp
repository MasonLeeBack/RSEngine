/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: Level.cpp

*/

#include "RSEngine.h"

Level* g_Level;
Root* g_Root;
std::vector<Instance*> LevelInstances;
bool IsLevelLoaded = false;

void Level::Unload()
{
	// Go ahead and halt the level's scripts.
	g_ScriptService->ClearLevelScripts();

	// Clean up the level instances.
	g_Root->ClearChildren();
	delete g_Root;
	g_Root = NULL;
	LevelInstances.clear();

	IsLevelLoaded = false;
}

void Level::NewInstance(Instance* userInstance, Instance* Parent)
{
	// Don't do anything if there's no level loaded!
	if (IsLevelLoaded)
	{
		Instance* myParent = Parent ? Parent : g_Root;
		userInstance->Parent = myParent;
		AddInstance(userInstance);
	}
}

void Level::SetRoot(Root* root)
{
	g_Root = root;
}

void Level::ClearRoot()
{
	g_Root = NULL;
}

//
// LevelInitialize: Places the client root and client camera on the level.
//
void Level::LevelInitialize()
{
	Root* nRoot = new Root;
	SetRoot(nRoot);
	AddInstance(nRoot);
	
	Camera* nCamera = new Camera;
	nCamera->Parent = g_Root;
	AddInstance(nCamera);

	IsLevelLoaded = true;
}

bool Level::Init()
{
	Level* l_Level = new Level;
	g_Level = l_Level;

	return true;
}

bool Level::LoadLevel(std::string FileName)
{
	return false;
}

bool Level::LoadTestLevel()
{
	if (IsLevelLoaded)
		Level::Unload();

	// Make sure to initialize the level first, otherwise, our 
	// engine will crash! (Camera and Root are added to the Instance list)
	LevelInitialize();

	Part* myPart = new Part;
	myPart->Size.init(10, 1, 10);
	myPart->Location.init(0, 0, 0);
	NewInstance(myPart, NULL);

	std::cout << "Loaded test level!\n";

#ifdef _DEBUG
	std::cout << "Loaded objects: " << LevelInstances.size() << "\n";

	std::vector<Instance*>::iterator it;
	for (it = LevelInstances.begin(); it < LevelInstances.end(); it++)
	{
		std::cout << ((InstanceChild*)*it)->ClassName << "\n";
	}
#endif

	return true;
}

