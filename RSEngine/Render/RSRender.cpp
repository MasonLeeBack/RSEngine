/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: RSRender.cpp

*/

#include <RSEngine.h>

namespace rs {

	IDXGISwapChain* pdx_SwapChain;
	ID3D11Device* pdx_Device;
	ID3D11DeviceContext* pdx_DeviceContext;
	ID3D11RenderTargetView*	pdx_RenderTargetView;
	Render* Renderer;

	std::vector<RenderPipeline*> pipelines;

	bool Render::Initialize()
	{
		HRESULT hr;

		// Enumerate available DX11 adapters
		IDXGIFactory* pFactory = NULL;
		CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pFactory);

		IDXGIAdapter* pAdapter;
		std::vector <IDXGIAdapter*> vAdapters;
		UINT i = 0;
		while (pFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND)
		{
			vAdapters.push_back(pAdapter);
		
			++i;
		}

		// Figure out DirectX feature levels
		const D3D_FEATURE_LEVEL FeatureLevels[]{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_11_1
		};

		DXGI_MODE_DESC BufferDesc;
		ZeroMemory(&BufferDesc, sizeof(DXGI_MODE_DESC));

		BufferDesc.Width = g_resX;
		BufferDesc.Height = g_resY;
		BufferDesc.RefreshRate.Numerator = 60;
		BufferDesc.RefreshRate.Denominator = 1;
		BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;


		DXGI_SWAP_CHAIN_DESC SwapChainDesc;
		ZeroMemory(&SwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

		// Updated by Render::UpdateSwapChainResolution
		SwapChainDesc.BufferDesc = BufferDesc;
		SwapChainDesc.SampleDesc.Count = 1;
		SwapChainDesc.SampleDesc.Quality = 0;
		SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		SwapChainDesc.BufferCount = 1;
		SwapChainDesc.OutputWindow = g_hWnd;
		SwapChainDesc.Windowed = 0; // Engine settings will change this (as well as UpdateSwapChainResolution)
		SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;


		// Create the device.
		hr = D3D11CreateDeviceAndSwapChain(
			NULL, // Use the PADAPTER enumeration later when we introduce settings
			D3D_DRIVER_TYPE_HARDWARE,
			NULL,
			NULL,
			NULL, // Feature levels is ready for use, however we're keeping things simple for now
			NULL,
			D3D11_SDK_VERSION,
			&SwapChainDesc,
			&pdx_SwapChain,
			&pdx_Device,
			NULL,
			&pdx_DeviceContext
			);
		if (FAILED(hr))
		{
			RSThrowError(L"DirectX 11 failed to initialize.");
			return false;
		}

		// Set up the viewport
		D3D11_VIEWPORT viewport;
		viewport.Height = (float)g_resY;
		viewport.Width = (float)g_resX;
		viewport.MinDepth = 0;
		viewport.MaxDepth = 1;

		pdx_DeviceContext->RSSetViewports(1, &viewport);

		ID3D11Texture2D* BackBuffer;
		hr = pdx_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&BackBuffer);
		if (FAILED(hr))
		{
			RSThrowError(L"Failed to get the backbuffer.");
			return false;
		}

		// BackBuffer Render Target
		hr = pdx_Device->CreateRenderTargetView(BackBuffer, NULL, &pdx_RenderTargetView);
		if (FAILED(hr))
		{
			RSThrowError(L"Could not create backbuffer render target.");
			return false;
		}

		//Temp
		pdx_DeviceContext->OMSetRenderTargets(1, &pdx_RenderTargetView, nullptr);

		// Clean up
		BackBuffer->Release();
		pFactory->Release();
		vAdapters.clear();

		return true;
	}

	bool Render::UpdateSwapChainResolution()
	{
		return false;
	}

	void Render::Shutdown()
	{
		pdx_Device->Release();
		delete pdx_Device;
		pdx_Device = 0;

		pdx_DeviceContext->Release();
		delete pdx_DeviceContext;
		pdx_DeviceContext = 0;

		pdx_SwapChain->Release();
		delete pdx_SwapChain;
		pdx_SwapChain = 0;

		pdx_RenderTargetView->Release();
		delete pdx_RenderTargetView;
		pdx_RenderTargetView = 0;
		
		// And any subcomponents of the renderer...

	}

	void Render::RenderScene()
	{
		// At least give us some sanity that our renderer works
		float rgba[4] = { 1.0f, 0.0f, 0.0f, 0.0f };

		pdx_DeviceContext->ClearRenderTargetView(pdx_RenderTargetView, rgba);
		pdx_SwapChain->Present(0, 0);
	}

	void Render::RenderAllPipelines()
	{
		
	}

}