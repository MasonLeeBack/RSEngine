/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: RenderInstance.cpp

*/

#include <RSEngine.h>

namespace rs {

	// Rewrite this later. This is sort of hack-y for right now.
	bool Render::RenderInstance(Instance* myInstance)
	{
		bool CanBeRendered = false;

		// Part specific stuff
		bool IsPart = false;
		Vector3 Size = { 0,0,0 };
		Vector3 Position = { 0,0,0 };


		// First we have to see what sort of instance this is.
		if (myInstance->ClassName == "Part")
		{
			// We can go ahead and start rendering with the Size/Position modifiers, since this isn't a mesh
			Size = ((Part*)myInstance)->Size;
			Position = ((Part*)myInstance)->Location;

			CanBeRendered = true; // Yeps!
			IsPart = true;
		}

		if (CanBeRendered)
		{
			RenderPipeline* myPipeline = new RenderPipeline;

			if (IsPart)
			{
				LoadShader(ST_PixelShader,	myPipeline,	"PartShaderVertex.cso");
				LoadShader(ST_VertexShader, myPipeline, "PartShaderPixel.cso");
			}


			pipelines.push_back(myPipeline);

			return true; 
		}

		return false;
	}

}