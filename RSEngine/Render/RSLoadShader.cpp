/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: RenderInstance.cpp

*/

#include <RSEngine.h>

namespace rs {

	VERTEXSHADER_MAP VertexMap;
	PIXELSHADER_MAP PixelMap;
	INPUTLAYOUT_MAP	LayoutMap;

	bool Render::LoadShader(SHADER_TYPE shaderType, RenderPipeline* pipeline, const char* filename)
	{
		if (shaderType == ST_PixelShader)
		{
			if (PixelMap.find(filename) == PixelMap.end())
			{
				ID3D11PixelShader* PixShader;
				HRESULT hr;

				// Create new pixel shader
				FILE* pixelShader;
				BYTE* bytes;

				size_t destSize = 4096;
				size_t bytesRead = 0;
				bytes = new BYTE[destSize];

				fopen_s(&pixelShader, filename, "rb");
				bytesRead = fread_s(bytes, destSize, 1, 4096, pixelShader);

				hr = pdx_Device->CreatePixelShader(
					bytes,
					bytesRead,
					nullptr,
					&PixShader
					);
				if (FAILED(hr))
					return false;

				delete bytes;

				PixelMap.insert(std::pair<const char*, ID3D11PixelShader*>(filename, PixShader));
				pipeline->PixelShader = PixShader;
			}
			else {
				pipeline->PixelShader = PixelMap.find(filename)->second;
			}
		}

		if (shaderType == ST_VertexShader )
		{
			if (VertexMap.find(filename) != VertexMap.end())
			{
				ID3D11VertexShader* VtxShader;
				HRESULT hr;

				// Create new vertex shader
				FILE* vertexShader;
				BYTE* bytes;

				size_t destSize = 4096;
				size_t bytesRead = 0;
				bytes = new BYTE[destSize];

				fopen_s(&vertexShader, filename, "rb");
				bytesRead = fread_s(bytes, destSize, 1, 4096, vertexShader);

				hr = pdx_Device->CreateVertexShader(
					bytes,
					bytesRead,
					nullptr,
					&VtxShader
					);

				ID3D11InputLayout* InputLayout;

				// This should not be defined here. But keep it for right now.
				D3D11_INPUT_ELEMENT_DESC layoutDesc[] =
				{
					{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,
					0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
					{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT,
					0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				};

				hr = pdx_Device->CreateInputLayout(
					layoutDesc,
					_ARRAYSIZE(layoutDesc),
					bytes,
					bytesRead,
					&InputLayout
					);

				delete bytes;

				LayoutMap.insert(std::pair<const char*, ID3D11InputLayout*>(filename, InputLayout));
				pipeline->InputLayout = InputLayout;

				VertexMap.insert(std::pair<const char*, ID3D11VertexShader*>(filename, VtxShader));
				pipeline->VertexShader = VtxShader;
			}
			else {
				pipeline->VertexShader = VertexMap.find(filename)->second;
			}
		}

		return true;
	}

}