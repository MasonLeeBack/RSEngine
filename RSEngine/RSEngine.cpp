/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: PolyEngine.cpp

*/

#include "RSEngine.h"


#ifdef _WIN32
INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hDlg, wParam);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, wParam);
			return TRUE;
		}
	case WM_CLOSE:
		return TRUE;
	case WM_DESTROY:
		PostQuitMessage(0);
		return FALSE;
	}
	return FALSE;
}

#ifdef _CONFIG_DIALOG
bool PolySettingsDialog()
{
	HWND hDlg = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_CONFIG), NULL, (DLGPROC)DialogProc);
	ShowWindow(hDlg, g_iCmdShow);

	BOOL ret;
	MSG msg;
	while ((ret = GetMessage(&msg, 0, 0, 0)) != 0) {
		if (ret == -1)
			return -1;

		if (!IsDialogMessage(hDlg, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return true;
}
#endif

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, INT iCmdShow)
{
	//
	// Go ahead and assign the HINSTANCE to the global in the window manager.
	//
	g_hInstance = hInstance;
	g_iCmdShow = iCmdShow;
	g_CommandLine = lpCmdLine;

	// To do: Set up external render mode setup! For now we just edit it from
	// this source file.

	renderMode = RM_D3D11;

	// Initialization

	INT xRes = GetPrivateProfileIntA("RSEngine", "ScreenResolution_X", 800, "C:\\Engine.ini");
	INT yRes = GetPrivateProfileIntA("RSEngine", "ScreenResolution_Y", 600, "C:\\Engine.ini");

#ifdef _CONFIG_DIALOG
	PolySettingsDialog();
#endif // _CONFIG_DIALOG

	Window l_Window;
	if (!l_Window.Init(xRes, yRes)) // To do: Implement resolution from INI and command line
	{
		RSThrowError(L"Could not create the window.\n");
	}
	Game l_Game;
	if (!l_Game.Init())
	{
		RSThrowError(L"Game failed to initialize.\n");
	}

	return 0;
}
#endif