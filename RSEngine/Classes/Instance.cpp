/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: Instance.cpp

*/

#include "RSEngine.h"

Instance * Instance::ScanChildren()
{
	return nullptr;
}

//
// Scan for children and remove them.
//
void Instance::ClearChildren()
{
	for (std::vector<Instance*>::iterator it = LevelInstances.begin();
		it < LevelInstances.end(); it++)
	{
		if (((InstanceChild*)*it)->Parent == this)
		{
			// See if it has children and remove them, and then
			// remove the instance.
			((InstanceChild*)*it)->ClearChildren();
			delete *it;
			*it = NULL;
			LevelInstances.erase(it);
		}
	}
}

//
// Removes the instance. It also removes the children.
//
void Instance::Remove()
{
	// Just call ClearChildren and delete the instance after.
	Instance::ClearChildren();
	
	// HACKHACK: We can't just delete "this"
	for (std::vector<Instance*>::iterator it = LevelInstances.begin();
		it < LevelInstances.end(); it++)
	{
		if (((InstanceChild*)*it) == this)
		{
			LevelInstances.erase(it);
			delete *it;
			*it = NULL;
		}
	}
}