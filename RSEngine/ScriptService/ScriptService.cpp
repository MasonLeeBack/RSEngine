/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: ScriptService.cpp

*/

#include "RSEngine.h"

ScriptService* g_ScriptService;
bool DefaultScript_Running = true;

bool ScriptService::Init()
{
	ScriptService* l_ScriptService = new ScriptService;
	g_ScriptService = l_ScriptService;

	return g_ScriptService->InitPhase1();
}

void ScriptService::Shutdown()
{
	delete g_ScriptService;
	g_ScriptService = 0;
}

void ScriptService::Update()
{
	if (DefaultScript_Running) {
		// this is the default script 
	}
}

void ScriptService::ClearLevelScripts()
{
	if (DefaultScript_Running)
		DefaultScript_Running = false;
}

bool ScriptService::InitPhase1()
{
	return true;
}
