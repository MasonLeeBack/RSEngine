/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: Level.h

*/

#pragma once
#ifndef _Level_h_
#define _Level_h_

#define AddInstance(x) LevelInstances.push_back(x)


extern std::vector<Instance*> LevelInstances;
extern bool IsLevelLoaded;

class Level {
public:
	bool Init();

	bool LoadLevel(std::string FileName);
	bool LoadTestLevel();
	void Unload();
	void NewInstance(Instance* userInstance, Instance* Parent);
private:
	void LevelInitialize();
	void SetRoot(Root* root);
	void ClearRoot();

};

extern Level* g_Level;

#endif