/*

PolyEngine
Copyright (c) 2017 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: Gamepad.h

*/

#pragma once
#ifndef _Gamepad_h_
#define _Gamepad_h_

enum {
  GAMEPAD_INPUT_XBOX,
  GAMEPAD_INPUT_DS,
  GAMEPAD_INPUT_UNIQUE
};

typedef enum {
  POLY_GPD_BTN_ACTION1,
  POLY_GPD_BTN_ACTION2,
  POLY_GPD_BTN_ACTION3,
  POLY_GPD_BTN_ACTION4,
  POLY_GPD_BTN_START,
  POLY_GPD_BTN_SELECT,
  POLY_GPD_BTN_TRIGGER1,
  POLY_GPD_BTN_TRIGGER2,
  POLY_GPD_BTN_BUMPER1,
  POLY_GPD_BTN_BUMPER2,
  POLY_GPD_BTN_DPADUP,
  POLY_GPD_BTN_DPADDOWN,
  POLY_GPD_BTN_DPADLEFT,
  POLY_GPD_BTN_DPADRIGHT,
  POLY_GPD_BTN_LTHUMBUP,
  POLY_GPD_BTN_LTHUMBDOWN,
  POLY_GPD_BTN_LTHUMBLEFT,
  POLY_GPD_BTN_LTHUMBRIGHT,
  POLY_GPD_BTN_LTHUMBPRESS,
  POLY_GPD_BTN_RTHUMBUP,
  POLY_GPD_BTN_RTHUMBDOWN,
  POLY_GPD_BTN_RTHUMBLEFT,
  POLY_GPD_BTN_RTHUMBRIGHT,
  POLY_GPD_BTN_RTHUMBPRESS,  
  POLY_GPD_BTN_UNIQUE
}POLY_GAMEPAD_BUTTONS;

#endif
