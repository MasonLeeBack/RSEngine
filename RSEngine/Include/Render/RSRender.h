/*

RSEngine
Copyright (c) 2018 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: RSRender.h

*/

#pragma once
#ifndef _RSRender_h_
#define _RSRender_h_

namespace rs {

	struct vertex {
		float pos[3]; // DirectX uses the left-handed Z coordinate system
		float uv[2];
	};

	typedef std::map<const char*, ID3D11VertexShader*>	VERTEXSHADER_MAP;
	typedef std::map<const char*, ID3D11PixelShader*>	PIXELSHADER_MAP;
	typedef std::map<const char*, ID3D11InputLayout*>	INPUTLAYOUT_MAP;

	extern VERTEXSHADER_MAP VertexMap;
	extern PIXELSHADER_MAP	PixelMap;
	extern INPUTLAYOUT_MAP	LayoutMap;

	// The downside to our vertex and pixel shader storage implementation is that we can't use the same file names, but that's okay
	struct RenderPipeline {
		ID3D11VertexShader*	VertexShader; 
		ID3D11InputLayout*	InputLayout;
		ID3D11PixelShader*	PixelShader;
		ID3D11Buffer*		VertexBuffer;
	};

	extern std::vector<RenderPipeline*> pipelines;

	typedef enum SHADER_TYPE {
		ST_PixelShader,
		ST_VertexShader
	}ShaderType;

	class Render {
	public:
		bool Initialize();
		bool UpdateSwapChainResolution();
		void Shutdown();
		void RenderScene();

		bool RenderInstance(Instance* myInstance);

	private:
		bool LoadShader(SHADER_TYPE shaderType, RenderPipeline* pipeline, const char* filename);
		void RenderAllPipelines();
	};

	extern IDXGISwapChain*			pdx_SwapChain;
	extern ID3D11Device*			pdx_Device;
	extern ID3D11DeviceContext*		pdx_DeviceContext;
	extern ID3D11RenderTargetView*	pdx_RenderTargetView;

	extern Render* Renderer;
}

#ifdef _USE_FBX_SDK_
#include "LoadModel_FBX.h"
#endif

#define Renderer_Initialize()					rs::Renderer->Initialize()
#define Renderer_UpdateSwapChainResolution()	rs::Renderer->UpdateSwapChainResolution()
#define Renderer_Shutdown()						rs::Renderer->Shutdown()
#define Renderer_RenderInstance(x)				rs::Renderer->RenderInstance(x)
#define Renderer_RenderScene()					rs::Renderer->RenderScene()

#endif