/*

PolyEngine
Copyright (c) 2017 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: RareXInput.cpp
Notes: Thanks, Katy, for examples of how to implement XInput

*/

#include "RSEngine.h"
#include <Input\RareXInput.h> // Not included in the main header for reasons

std::vector<int> conn_controllers;

bool RareXInput::ScanControllers()
{
	for (int i = 0; i < XUSER_MAX_COUNT; i++)
	{
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		if (XInputGetState(i, &state) == ERROR_SUCCESS)
		{
			conn_controllers.push_back(i);
		}
	}

	return (conn_controllers.size() != 0);
}

int RareXInput::ControllerCount()
{
	return static_cast<int>(conn_controllers.size());
}

bool RareXInput::IsInputPressed(POLY_GAMEPAD_BUTTONS button)
{
	WORD XIButton;
	// probably an easier way to do this, but for right now just
	// do this
	switch(button)
	{
		case POLY_GPD_BTN_ACTION1:
			XIButton = XINPUT_GAMEPAD_A;
			break;
		case POLY_GPD_BTN_ACTION2:
			XIButton = XINPUT_GAMEPAD_B;
			break;
		case POLY_GPD_BTN_ACTION3:
			XIButton = XINPUT_GAMEPAD_X;
			break;
		case POLY_GPD_BTN_ACTION4:
			XIButton = XINPUT_GAMEPAD_Y;
			break;
		case POLY_GPD_BTN_DPADUP:
			XIButton = XINPUT_GAMEPAD_DPAD_UP;
			break;
		case POLY_GPD_BTN_DPADDOWN:
			XIButton = XINPUT_GAMEPAD_DPAD_DOWN;
			break;
		case POLY_GPD_BTN_DPADLEFT:
			XIButton = XINPUT_GAMEPAD_DPAD_LEFT;
			break;
		case POLY_GPD_BTN_DPADRIGHT:
			XIButton = XINPUT_GAMEPAD_DPAD_RIGHT;
			break;
		case POLY_GPD_BTN_LTHUMBPRESS:
			XIButton = XINPUT_GAMEPAD_LEFT_THUMB;
			break;
		case POLY_GPD_BTN_RTHUMBPRESS:
			XIButton = XINPUT_GAMEPAD_RIGHT_THUMB;
			break;
		case POLY_GPD_BTN_BUMPER1:
			XIButton = XINPUT_GAMEPAD_LEFT_SHOULDER;
			break;
		case POLY_GPD_BTN_BUMPER2:
			XIButton = XINPUT_GAMEPAD_RIGHT_SHOULDER;
			break;
		default:
			break;
	}
	
	return false;
}
