/*

PolyEngine
Copyright (c) 2017 Mason Lee Back

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

File name: PolyInput.cpp

*/

#include "RSEngine.h"
Input* g_Input;

#ifdef _WIN32
#include <Input\RareXInput.h>
RareXInput rxInput;
#endif

bool Input::Init()
{
	Input* l_Input = new Input;
	g_Input = l_Input;

	return g_Input->InitPhase1();
}

void Input::Shutdown()
{
	delete g_Input;
	g_Input = 0;
}

bool Input::InitPhase1()
{
#ifdef _WIN32
	//
	// Try seeing if we have any Xbox controllers
	//

	if (rxInput.ScanControllers())
	{
		std::cout << "We have controllers!\n" << "Controller count: " << rxInput.ControllerCount();
	}
#endif

	return true;
}

int Input::GetInputType()
{
	// Nothing for now.
	return IPTYP_NULL;
}

int Input::Update()
{
	return 0;
}

// 
bool Input::IsInputPressed(int VirtualKey)
{
#ifdef _WIN32
	if (GetKeyState(VirtualKey) & 0x8000)
		return true;

	if (rxInput.ControllerCount() != 0)
		if (rxInput.IsInputPressed((POLY_GAMEPAD_BUTTONS)VirtualKey)) // fix this later
			return true;
#endif
	return false;
}
